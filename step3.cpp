#include <algorithm>
#include <iostream>
#include <variant>
#include <vector>


struct ArgumentParser {
    struct Argument {
        // define the Argument struct
    };

    struct ArgumentVisitor {
        // write a visitor struct
    };

    std::vector<Argument> args_without_values;
    std::vector<Argument> args_with_value;

public:
    void add_option_string(std::string_view name, std::string &value, std::string_view long_option, char short_option = 0)
    {
        args_with_value.emplace_back(name, &value, long_option, short_option);
    }

    void add_option_int(std::string_view name, int &value, std::string_view long_option, char short_option = 0)
    {
        args_with_value.emplace_back(name, &value, long_option, short_option);
    }

    void add_option_double(std::string_view name, double &value, std::string_view long_option, char short_option = 0)
    {
        args_with_value.emplace_back(name, &value, long_option, short_option);
    }

    void add_option(std::string_view name, bool &value, std::string_view long_option, char short_option = 0)
    {
        args_without_values.emplace_back(name, &value, long_option, short_option);
    }

    void parse(int argc, char** argv)
    {
        // Parse the arguments from arg_without_values and args_with_value here
    }
};


int main(int argc, char** argv)
{
    std::string name = "";
    int age = -1;
    bool debug = false;
    bool bool1 = false;
    bool bool2 = false;

    ArgumentParser arg_parser;
    arg_parser.add_option_int("age", age, "age", 'a');
    arg_parser.add_option_string("name", name, "name");
    arg_parser.add_option("debug", debug, "debug", 'd');
    arg_parser.add_option("opt1", bool1, "opt1", '1');
    arg_parser.add_option("opt2", bool2, "opt2", '2');
    arg_parser.parse(argc, argv);

    std::cout << "Result:\nName: " << name
        << "\nAge: " << age
        << "\nDebug: " << (debug?"true":"false")
        << "\nOption1: " << (bool1?"true":"false")
        << "\nOption2: " << (bool2?"true":"false") << std::endl;

    return 0;
}
